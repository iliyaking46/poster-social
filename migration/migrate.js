const path = require('path')
const envPath = path.resolve(process.cwd(), '.env.local')
require('dotenv').config({ path: envPath })
const mysql = require('serverless-mysql')

const db = mysql({
    config: {
        host: process.env.MYSQL_HOST,
        database: process.env.MYSQL_DATABASE,
        user: process.env.MYSQL_USERNAME,
        password: process.env.MYSQL_PASSWORD,
    },
})

const query = async (q) => {
    try {
        const results = await db.query(q)
        await db.end()
        return results
    } catch (e) {
        throw Error(e.message)
    }
}

const createUsers = async () => {
    await query(`
  CREATE TABLE IF NOT EXISTS users (
    id VARCHAR(255) NOT NULL UNIQUE PRIMARY KEY,
    name VARCHAR(255) NOT NULL,
    about TEXT NOT NULL,
    created_at TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
    updated_at 
      TIMESTAMP
      NOT NULL
      DEFAULT CURRENT_TIMESTAMP 
      ON UPDATE CURRENT_TIMESTAMP
  )
  `)
}

const createPosts = async () => {
    await query(`
    CREATE TABLE IF NOT EXISTS posts (
      id INT AUTO_INCREMENT PRIMARY KEY,
      author_id VARCHAR(255) NOT NULL,
      content TEXT NOT NULL,
      reply_on INT,
      created_at TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
      updated_at
        TIMESTAMP
        NOT NULL
        DEFAULT CURRENT_TIMESTAMP 
        ON UPDATE CURRENT_TIMESTAMP,
      FOREIGN KEY (author_id) REFERENCES users(id),
      FOREIGN KEY (reply_on) REFERENCES posts(id)
    )
    `)
}

// Create "entries" table if doesn't exist
async function migrate() {
    try {
        // await query(`DROP TABLE posts`)
        // await query(`DROP TABLE users`)
        // console.log('tables succesfully dropped')
        await createUsers();
        console.log('migration on users ran successfully')

        await createPosts();
        console.log('migration on posts ran successfully')

        console.log('all migration ran successfully')
    } catch (e) {
        console.error(e)
        process.exit(1)
    }
}

migrate().then(() => process.exit())
