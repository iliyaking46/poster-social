import { createPostApi } from 'helpers/posts'
import { PostCreate } from 'interfaces/Post'
import { useState, useEffect } from 'react'
import { mutate } from 'swr'

export const PostForm = () => {
  const [author_id, setTitle] = useState('')
  const [content, setContent] = useState('')
  const [submitting, setSubmitting] = useState(false)

  const submitHandler = async (e) => {
    e.preventDefault()
    setSubmitting(true)
    try {
      const data = await createPostApi({ author_id, content } as PostCreate)
      console.log(data)
      setSubmitting(false)

      if (data.message) throw Error(data.message)

      mutate('/api/posts')
      setTitle('')
      setContent('')

    } catch (e) {
      console.error(e)
    }
  }

  return (
    <form onSubmit={submitHandler}>
      <div className="my-4">
        <label htmlFor="title">
          <h3 className="font-bold">Title</h3>
        </label>
        <input
          id="title"
          className="shadow border rounded w-full"
          type="text"
          name="title"
          value={author_id}
          onChange={(e) => setTitle(e.target.value)}
        />
      </div>
      <div className="my-4">
        <label htmlFor="content">
          <h3 className="font-bold">Content</h3>
        </label>
        <textarea
          className="shadow border resize-none focus:shadow-outline w-full h-48"
          id="content"
          name="content"
          value={content}
          onChange={(e) => setContent(e.target.value)}
        />
      </div>
      <button disabled={submitting || !content || !author_id} type="submit">
        {submitting ? 'Saving ...' : 'Save'}
      </button>
    </form>
  )
}
