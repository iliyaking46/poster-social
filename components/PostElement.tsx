import { deletePostApi } from "helpers/posts"
import { Post } from "interfaces/Post"
import { mutate } from "swr"

export const PostElement = ({author_id, content, id}: Post) => {
    const deletePost = async () => {
        try {
            await deletePostApi(id)
            mutate('/api/posts')

        } catch (e) {
            console.error(e)
        }
    }
    return (
        <div style={{ marginBottom: '1em' }}>
            <h3>{author_id} <span onClick={deletePost} style={{cursor: 'pointer', float: 'right', fontWeight: 'normal'}}>X</span></h3>
            <p>{content}</p>
        </div>
    )
}