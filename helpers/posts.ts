import { deletePost } from '@/database/posts'
import { PostCreate } from 'interfaces/Post'
import useSWR from 'swr'

function fetcher(url: string, config?: Object) {
  return window.fetch(url, config).then((res) => res.json())
}

export const usePosts = () => {
  const { data, error } = useSWR(`/api/posts`, fetcher)

  return {
    posts: data,
    isLoading: !error && !data,
    isError: error,
  }
}

export const deletePostApi = async (id: number) => {
  const result = await fetcher(`/api/posts/${id}`, {
    method: 'DELETE',
    headers: {
      'Content-Type': 'application/json',
    }
  })
  if (result.affectedRows === 0) throw new Error(`cannot delete post with id = ${id}`)
  return result
}

export const createPostApi = async ({ author_id, content }: PostCreate) => {
  const result = await fetcher('/api/posts', {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
    },
    body: JSON.stringify({
      author_id,
      content,
    }),
  })
  return result
}