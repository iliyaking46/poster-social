import { NextApiHandler } from 'next'
import { deletePost, getPostById } from '@/database/posts'
import { type } from 'os';

const handler: NextApiHandler = async (req, res) => {
    const { id } = req.query;
    const method = req.method;
    try {
        if (typeof id !== 'string') throw new Error('id must be a string')

        if (req.method === 'DELETE') {
            const result = await deletePost(id)
            return res.json(result)
        }

        const results = await getPostById(id)

        return res.json(results)
    } catch (e) {
        res.status(500).json({ message: e.message })
    }
}
    
export default handler
