import { NextApiHandler } from 'next'
import { getPosts, setPost } from '@/database/posts'

const handler: NextApiHandler = async (req, res) => {
    if (req.method === 'POST') {
        const { author_id, content, reply_on = null } = req.body;

        try {
            if (!author_id || !content) throw new Error('empty required fields')
            const result = await setPost({ author_id, content, reply_on }) as {insertId: number}

            return res.json({id: result.insertId})
        } catch (e) {
            return res.status(500).json({ message: e.message })
        }
    }

    try {
        const results = await getPosts()

        return res.json(results)
    } catch (e) {
        res.status(500).json({ message: e.message })
    }
}

export default handler
