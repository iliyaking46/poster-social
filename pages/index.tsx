import { useEffect, useState } from 'react';
import Head from 'next/head'
import { Post } from 'interfaces/Post';
import styles from 'styles/Home.module.css'
import { usePosts } from 'helpers/posts';
import { PostForm } from 'components/PostForm';
import { PostElement } from 'components/PostElement';

type Props = {
  posts: Post[]
}

export default function Home() {
  const { posts, isLoading } = usePosts()

  return (
    <div className={styles.container}>
      <Head>
        <title>Social Poster</title>
        <link rel="icon" href="/favicon.ico" />
      </Head>
      <div className={styles.main}>
        <PostForm />
        {isLoading
          ? 'loading posts'
          : posts.map((post) => (
            <PostElement key={post.id} {...post} />
          ))}
      </div>
    </div>
  )
}
