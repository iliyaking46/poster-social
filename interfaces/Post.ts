export type PostCreate = {
    author_id: string,
    content: string,
    reply_on?: number,
}

export type Post = PostCreate & {
    id: number,
    created_at: string,
    updated_at: string
}