export type User = {
    id: string,
    name: string,
    about: string,
    created_at: string,
    updated_at: string
}