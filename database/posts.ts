import { query } from './db';

const SELECT_POSTS = 'SELECT * FROM posts ORDER BY created_at DESC';
const INSERT_POST = `INSERT INTO posts (author_id, content, reply_on) VALUES (?, ?, ?)`
const DELETE_POST = `DELETE FROM posts WHERE id = ?`
const SELECT_POSTS_BY_ID = `
    SELECT * FROM posts 
    WHERE id = ?
    ORDER BY created_at DESC
`;

export const getPosts = async () => {
    const result = await query(SELECT_POSTS);
    return result;
}

export const getPostById = async (id: string) => {
    const result = await query(SELECT_POSTS_BY_ID, id);
    return result;
}

export const setPost = async ({author_id, content, reply_on}) => {
    const result = await query(INSERT_POST, [author_id, content, reply_on]);
    return result;
}

export const deletePost = async (id: string) => {
    const result = await query(DELETE_POST, id);
    return result;
}